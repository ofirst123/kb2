<?php

namespace app\models;

use Yii;
use dosamigos\taggable\Taggable;
use yii\helpers\Url;
use kartik\select2\Select2;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property int $frequncy
 * @property string $name
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frequency', 'name'], 'required'],
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'Frequency',
            'name' => 'Name',
        ];
    }

    public static function findAllByName($name)
    {
        return Tags::find()
            ->where(['like', 'name', $name])->limit(50)->all();
    }

  //  public function getTags()
   // {
  //      return $this->hasMany(Tags::className(), ['id' => 'tags_id'])->viaTable('tags_assn', ['article_id' => 'id']);//דוגמא לקישור של רבים לרבים
//}


}
