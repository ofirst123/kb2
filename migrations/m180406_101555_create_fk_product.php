<?php

use yii\db\Migration;

/**
 * Class m180406_101555_create_fk_product
 */
class m180406_101555_create_fk_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-product-category_id',
            'product',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180406_101555_create_fk_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180406_101555_create_fk_product cannot be reverted.\n";

        return false;
    }
    */
}
